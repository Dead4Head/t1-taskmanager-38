package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.IUserRepository;
import ru.t1.amsmirnov.taskmanager.api.service.*;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.UserNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.EmailEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.LoginEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.PasswordEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.user.EmailExistException;
import ru.t1.amsmirnov.taskmanager.exception.user.LoginExistException;
import ru.t1.amsmirnov.taskmanager.model.User;
import ru.t1.amsmirnov.taskmanager.repository.UserRepository;
import ru.t1.amsmirnov.taskmanager.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.projectService = projectService;
        this.taskService = taskService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public IUserRepository getRepository(@NotNull Connection connection) {
        return new UserRepository(connection);
    }

    @NotNull
    @Override
    public User findOneByLogin(@Nullable final String login) throws AbstractException, SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            @Nullable final User user = userRepository.findOneByLogin(login);
            if (user == null) throw new UserNotFoundException();
            return user;
        }
    }

    @NotNull
    @Override
    public User findOneByEmail(@Nullable final String email) throws AbstractException, SQLException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            @Nullable final User user = userRepository.findOneByEmail(email);
            if (user == null) throw new UserNotFoundException();
            return user;
        }
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws AbstractException, SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            return userRepository.isLoginExist(login);
        }
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) throws AbstractException, SQLException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository userRepository = getRepository(connection);
            return userRepository.isEmailExist(email);
        }
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) throws AbstractException, SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmailExist(email)) throw new EmailExistException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        if (role != null) user.setRole(role);
        return add(user);
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException, SQLException {
        return create(login, password, email, null);
    }

    @NotNull
    @Override
    public User removeOne(@Nullable final User user) throws AbstractException, SQLException {
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        taskService.removeAll(userId);
        projectService.removeAll(userId);
        return super.removeOne(user);
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) throws AbstractException, SQLException {
        @NotNull final User user = findOneByLogin(login);
        return removeOne(user);
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) throws AbstractException, SQLException {
        @NotNull final User user = findOneByEmail(email);
        return removeOne(user);
    }

    @NotNull
    @Override
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException, SQLException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = findOneById(id);
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        return update(user);
    }

    @NotNull
    @Override
    public User updateUserById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws AbstractException, SQLException {
        @NotNull User user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return update(user);
    }

    @Override
    public void lockUserByLogin(final String login) throws AbstractException, SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final User user = findOneByLogin(login);
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUserByLogin(final String login) throws AbstractException, SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final User user = findOneByLogin(login);
        user.setLocked(false);
        update(user);
    }

}
