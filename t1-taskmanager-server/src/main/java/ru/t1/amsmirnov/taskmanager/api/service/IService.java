package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    M add(@Nullable M model) throws AbstractException, SQLException;

    @NotNull
    Collection<M> addAll(@Nullable Collection<M> models) throws AbstractException, SQLException;

    @NotNull
    Collection<M> set(@Nullable Collection<M> models) throws AbstractException, SQLException;

    @NotNull
    List<M> findAll() throws AbstractException, SQLException;

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator) throws AbstractException, SQLException;

    @NotNull
    M findOneByIndex(@Nullable Integer index) throws AbstractException, SQLException;

    @NotNull
    M findOneById(@Nullable String id) throws AbstractException, SQLException;

    @NotNull
    M update(@Nullable M model) throws AbstractException, SQLException;

    void removeAll() throws SQLException;

    void removeAll(@Nullable Collection<M> collection) throws SQLException;

    @NotNull
    M removeOne(@Nullable M model) throws AbstractException, SQLException;

    @NotNull
    M removeOneByIndex(@Nullable Integer index) throws AbstractException, SQLException;

    @NotNull
    M removeOneById(@Nullable String id) throws AbstractException, SQLException;

    int getSize() throws SQLException;

    boolean existById(@NotNull String id) throws AbstractException, SQLException;

}
