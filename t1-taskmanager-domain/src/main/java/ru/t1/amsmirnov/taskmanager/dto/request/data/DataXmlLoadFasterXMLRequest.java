package ru.t1.amsmirnov.taskmanager.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class DataXmlLoadFasterXMLRequest extends AbstractUserRequest {

    public DataXmlLoadFasterXMLRequest() {
    }

    public DataXmlLoadFasterXMLRequest(@Nullable String token) {
        super(token);
    }

}
