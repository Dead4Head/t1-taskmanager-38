package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.repository.ISessionRepository;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.enumerated.database.DBCost;
import ru.t1.amsmirnov.taskmanager.model.Session;

import java.sql.*;
import java.util.Collection;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @NotNull
    private final String tableName = DBCost.TABLE_SESSION.getDisplayName();

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return tableName;
    }

    @NotNull
    @Override
    protected Session fetch(@NotNull final ResultSet row) throws SQLException {
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setCreated(row.getTimestamp(DBCost.COLUMN_CREATED.getDisplayName()));
        session.setRole(Role.valueOf(row.getString(DBCost.COLUMN_ROLE.getDisplayName())));
        session.setUserId(row.getString(DBCost.COLUMN_USER_ID.getDisplayName()));
        return session;
    }

    @NotNull
    @Override
    public Session add(@NotNull final Session session) throws SQLException {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s) VALUES(?, ?, ?, ?)",
                getTableName(),
                DBCost.COLUMN_ID.getDisplayName(),
                DBCost.COLUMN_USER_ID.getDisplayName(),
                DBCost.COLUMN_CREATED.getDisplayName(),
                DBCost.COLUMN_ROLE.getDisplayName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setString(2, session.getUserId());
            statement.setTimestamp(3, new Timestamp(session.getCreated().getTime()));
            statement.setString(4, session.getRole().toString());
            statement.executeUpdate();
            connection.commit();
        }
        return session;
    }

    @NotNull
    @Override
    public Session add(
            @NotNull final String userId,
            @NotNull final Session session
    ) throws SQLException {
        session.setUserId(userId);
        return add(session);
    }

    @NotNull
    @Override
    public Collection<Session> addAll(@NotNull final Collection<Session> sessions) throws SQLException {
        for (final Session session : sessions) {
            add(session);
        }
        return sessions;
    }

    @NotNull
    @Override
    public Session update(@NotNull final Session session) throws SQLException {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ? WHERE %s = ?",
                getTableName(),
                DBCost.COLUMN_ROLE.getDisplayName(),
                DBCost.COLUMN_ID.getDisplayName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getRole().toString());
            statement.setString(2, session.getId());
            statement.executeUpdate();
            connection.commit();
        }
        return session;
    }

}
