package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.IProjectService;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.IndexIncorrectException;
import ru.t1.amsmirnov.taskmanager.exception.field.NameEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.repository.ProjectRepository;

import java.sql.Connection;
import java.sql.SQLException;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public IProjectRepository getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException, SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(userId, project);
        return project;
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException, SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        return update(project);
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException, SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();
        if (index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project project = findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        return update(project);
    }

    @NotNull
    @Override
    public Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException, SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Project project = findOneById(userId, id);
        if (status == null) return project;
        project.setStatus(status);
        return update(project);
    }

    @NotNull
    @Override
    public Project changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) throws AbstractException, SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();
        if (index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @NotNull final Project project = findOneByIndex(userId, index);
        if (status == null) return project;
        project.setStatus(status);
        return update(project);
    }

}