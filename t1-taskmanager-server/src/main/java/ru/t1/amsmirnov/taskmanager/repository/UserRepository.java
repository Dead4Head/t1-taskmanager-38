package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.IUserRepository;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.enumerated.database.DBCost;
import ru.t1.amsmirnov.taskmanager.enumerated.database.UserConst;
import ru.t1.amsmirnov.taskmanager.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final String tableName = DBCost.TABLE_USER.getDisplayName();

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected User fetch(@NotNull final ResultSet row) throws SQLException {
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString(UserConst.COLUMN_LOGIN.getColumnName()));
        user.setPasswordHash(row.getString(UserConst.COLUMN_PASSWORD.getColumnName()));
        user.setEmail(row.getString(UserConst.COLUMN_EMAIL.getColumnName()));
        user.setFirstName(row.getString(UserConst.COLUMN_FST_NAME.getColumnName()));
        user.setLastName(row.getString(UserConst.COLUMN_LST_NAME.getColumnName()));
        user.setMiddleName(row.getString(UserConst.COLUMN_MID_NAME.getColumnName()));
        user.setRole(Role.valueOf(row.getString(DBCost.COLUMN_ROLE.getDisplayName())));
        user.setLocked(row.getBoolean(UserConst.COLUMN_LOCKED.getColumnName()));
        return user;

    }

    @NotNull
    @Override
    public User add(@NotNull final User user) throws SQLException {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (id, %s, %s, %s, %s, %s, %s, %s, %s) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName(),
                UserConst.COLUMN_LOGIN.getColumnName(),
                UserConst.COLUMN_PASSWORD.getColumnName(),
                UserConst.COLUMN_EMAIL.getColumnName(),
                UserConst.COLUMN_FST_NAME.getColumnName(),
                UserConst.COLUMN_LST_NAME.getColumnName(),
                UserConst.COLUMN_MID_NAME.getColumnName(),
                DBCost.COLUMN_ROLE.getDisplayName(),
                UserConst.COLUMN_LOCKED.getColumnName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getFirstName());
            statement.setString(6, user.getLastName());
            statement.setString(7, user.getMiddleName());
            statement.setString(8, user.getRole().toString());
            statement.setBoolean(9, user.isLocked());
            statement.executeUpdate();
            connection.commit();
        }
        return user;
    }

    @NotNull
    @Override
    public Collection<User> addAll(@NotNull final Collection<User> users) throws SQLException {
        for (@NotNull final User user : users)
            add(user);
        return users;
    }

    @NotNull
    @Override
    public User update(@NotNull final User user) throws SQLException {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(),
                UserConst.COLUMN_LOGIN.getColumnName(),
                UserConst.COLUMN_PASSWORD.getColumnName(),
                UserConst.COLUMN_EMAIL.getColumnName(),
                UserConst.COLUMN_FST_NAME.getColumnName(),
                UserConst.COLUMN_LST_NAME.getColumnName(),
                UserConst.COLUMN_MID_NAME.getColumnName(),
                DBCost.COLUMN_ROLE.getDisplayName(),
                UserConst.COLUMN_LOCKED.getColumnName(),
                DBCost.COLUMN_ID.getDisplayName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getMiddleName());
            statement.setString(7, user.getRole().toString());
            statement.setBoolean(8, user.isLocked());
            statement.setString(9, user.getId());
            statement.executeUpdate();
            connection.commit();
        }
        return user;
    }


    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) throws SQLException {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1",
                getTableName(),
                UserConst.COLUMN_LOGIN.getColumnName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet result = statement.executeQuery();
            if (result.next())
                return fetch(result);
        }
        return null;
    }

    @Nullable
    @Override
    public User findOneByEmail(@NotNull final String email) throws SQLException {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1",
                getTableName(),
                UserConst.COLUMN_EMAIL.getColumnName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet result = statement.executeQuery();
            if (result.next())
                return fetch(result);
        }
        return null;
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) throws SQLException {
        return findOneByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) throws SQLException {
        return findOneByEmail(email) != null;
    }

    @NotNull
    @Override
    public String getTableName() {
        return tableName;
    }

}
