package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.sql.SQLException;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task>{

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException, SQLException;

    @NotNull
    Task updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException, SQLException;

    @NotNull
    Task updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws AbstractException, SQLException;

    @NotNull
    Task changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws AbstractException, SQLException;

    @NotNull
    Task changeStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    ) throws AbstractException, SQLException;

    @NotNull
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    ) throws AbstractException, SQLException;

}
