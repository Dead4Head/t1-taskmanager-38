package ru.t1.amsmirnov.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model) throws SQLException;

    @NotNull
    Collection<M> addAll(@NotNull Collection<M> models) throws SQLException;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws SQLException;

    @Nullable
    List<M> findAll() throws SQLException;

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator) throws SQLException;

    @Nullable
    M findOneById(@NotNull String id) throws SQLException;

    @Nullable
    M findOneByIndex(@NotNull Integer index) throws SQLException;

    @NotNull
    M update(@NotNull M model) throws SQLException;

    void removeAll() throws SQLException;

    void removeAll(@NotNull Collection<M> collection) throws SQLException;

    @NotNull
    M removeOne(@NotNull M model) throws SQLException;

    @Nullable
    M removeOneById(@NotNull String id) throws SQLException;

    @Nullable
    M removeOneByIndex(@NotNull Integer index) throws SQLException;

    int getSize() throws SQLException;

    boolean existById(@NotNull String id) throws SQLException;

}
