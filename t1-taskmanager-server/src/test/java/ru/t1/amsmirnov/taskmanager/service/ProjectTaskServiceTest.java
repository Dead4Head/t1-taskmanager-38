package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.IProjectService;
import ru.t1.amsmirnov.taskmanager.api.service.IProjectTaskService;
import ru.t1.amsmirnov.taskmanager.api.service.ITaskService;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ProjectNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.TaskNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.ProjectIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.TaskIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class ProjectTaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String NONE_STR = "---NONE---";

    @Nullable
    private static final String NULL_STR = null;

    @NotNull
    private final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(new PropertyService());

    @NotNull
    private static final IProjectService PROJECT_SERVICE = new ProjectService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskService TASK_SERVICE = new TaskService(CONNECTION_SERVICE);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(CONNECTION_SERVICE);

    @NotNull
    private final List<Task> bindedTasks = new ArrayList<>();

    @NotNull
    private final List<Task> unbindedTasks = new ArrayList<>();

    @NotNull
    private Project project;

    @Before
    public void initRepository() throws Exception {
        project = new Project();
        project.setName("Project");
        project.setDescription("Description");
        project.setUserId(USER_ALFA_ID);
        PROJECT_SERVICE.add(project);

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final Task task = new Task();
            task.setName("Task binded name: " + (NUMBER_OF_ENTRIES - i));
            task.setDescription("Task binded description: " + i);
            task.setUserId(USER_ALFA_ID);
            task.setProjectId(project.getId());
            TASK_SERVICE.add(task);
            bindedTasks.add(task);
        }

        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final Task task = new Task();
            task.setName("Task unbinded name: " + i);
            task.setDescription("Task unbinded description: " + i);
            task.setUserId(USER_ALFA_ID);
            TASK_SERVICE.add(task);
            unbindedTasks.add(task);
        }
    }

    @After
    public void clearRepositories() throws SQLException {
        PROJECT_SERVICE.removeAll();
        TASK_SERVICE.removeAll();
    }

    @Test
    public void testBindTaskToProject() throws AbstractException, SQLException {
        for (final Task task : unbindedTasks) {
            assertNull(task.getProjectId());
            projectTaskService.bindTaskToProject(USER_ALFA_ID, project.getId(), task.getId());
            assertEquals(project.getId(), TASK_SERVICE.findOneById(task.getId()).getProjectId());
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testBindTaskToProject_UserIdEmptyException_1() throws AbstractException, SQLException {
        projectTaskService.bindTaskToProject("", NONE_STR, NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testBindTaskToProject_UserIdEmptyException_2() throws AbstractException, SQLException {
        projectTaskService.bindTaskToProject(NULL_STR, NONE_STR, NONE_STR);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testBindTaskToProject_ProjectIdEmptyException_1() throws AbstractException, SQLException {
        projectTaskService.bindTaskToProject(USER_ALFA_ID, "", NONE_STR);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testBindTaskToProject_ProjectIdEmptyException_2() throws AbstractException, SQLException {
        projectTaskService.bindTaskToProject(USER_ALFA_ID, NULL_STR, NONE_STR);
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testBindTaskToProject_TaskIdEmptyException_1() throws AbstractException, SQLException {
        projectTaskService.bindTaskToProject(USER_ALFA_ID, NONE_STR, "");
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testBindTaskToProject_TaskIdEmptyException_2() throws AbstractException, SQLException {
        projectTaskService.bindTaskToProject(USER_ALFA_ID, NONE_STR, NULL_STR);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testBindTaskToProject_ProjectNotFoundException() throws AbstractException, SQLException {
        projectTaskService.bindTaskToProject(NONE_STR, project.getId(), NONE_STR);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testBindTaskToProject_TaskNotFoundException() throws AbstractException, SQLException {
        projectTaskService.bindTaskToProject(USER_ALFA_ID, project.getId(), NONE_STR);
    }

    @Test
    public void testRemoveProjectById() throws AbstractException, SQLException {
        assertNotEquals(0, TASK_SERVICE.findAllByProjectId(USER_ALFA_ID, project.getId()).size());
        assertTrue(PROJECT_SERVICE.existById(project.getId()));
        projectTaskService.removeProjectById(USER_ALFA_ID, project.getId());
        assertFalse(PROJECT_SERVICE.existById(project.getId()));
        assertEquals(0, TASK_SERVICE.findAllByProjectId(USER_ALFA_ID, project.getId()).size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveProjectById_UserIdEmptyException_1() throws AbstractException, SQLException {
        projectTaskService.removeProjectById("", NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testRemoveProjectById_UserIdEmptyException_2() throws AbstractException, SQLException {
        projectTaskService.removeProjectById(NULL_STR, NONE_STR);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testRemoveProjectById_ProjectIdEmptyException_1() throws AbstractException, SQLException {
        projectTaskService.removeProjectById(USER_ALFA_ID, "");
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testRemoveProjectById_ProjectIdEmptyException_2() throws AbstractException, SQLException {
        projectTaskService.removeProjectById(USER_ALFA_ID, NULL_STR);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testRemoveProjectById_ProjectNotFoundException() throws AbstractException, SQLException {
        projectTaskService.removeProjectById(USER_ALFA_ID, NONE_STR);
    }

    @Test
    public void testUnbindTaskFromProject() throws AbstractException, SQLException {
        for (final Task task : bindedTasks) {
            assertEquals(project.getId(), task.getProjectId());
            projectTaskService.unbindTaskFromProject(USER_ALFA_ID, project.getId(), task.getId());
            assertNull(TASK_SERVICE.findOneById(task.getId()).getProjectId());
        }
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUnbindTaskFromProject_UserIdEmptyException_1() throws AbstractException, SQLException {
        projectTaskService.unbindTaskFromProject("", NONE_STR, NONE_STR);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUnbindTaskFromProject_UserIdEmptyException_2() throws AbstractException, SQLException {
        projectTaskService.unbindTaskFromProject(NULL_STR, NONE_STR, NONE_STR);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testUnbindTaskFromProject_ProjectIdEmptyException_1() throws AbstractException, SQLException {
        projectTaskService.unbindTaskFromProject(USER_ALFA_ID, "", NONE_STR);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testUnbindTaskFromProject_ProjectIdEmptyException_2() throws AbstractException, SQLException {
        projectTaskService.unbindTaskFromProject(USER_ALFA_ID, NULL_STR, NONE_STR);
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testUnbindTaskFromProject_TaskIdEmptyException_1() throws AbstractException, SQLException {
        projectTaskService.unbindTaskFromProject(USER_ALFA_ID, NONE_STR, "");
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testUnbindTaskFromProject_TaskIdEmptyException_2() throws AbstractException, SQLException {
        projectTaskService.unbindTaskFromProject(USER_ALFA_ID, NONE_STR, NULL_STR);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUnbindTaskFromProject_ProjectNotFoundException() throws AbstractException, SQLException {
        projectTaskService.unbindTaskFromProject(NONE_STR, project.getId(), NONE_STR);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUnbindTaskFromProject_TaskNotFoundException() throws AbstractException, SQLException {
        projectTaskService.unbindTaskFromProject(USER_ALFA_ID, project.getId(), NONE_STR);
    }

}
