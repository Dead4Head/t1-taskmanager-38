package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.IDBProperty;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IDBProperty databaseProperty;

    public ConnectionService(@NotNull final IDBProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
    }

    @NotNull
    @Override
    public Connection getConnection() {
        try {
            @NotNull final String username = databaseProperty.getDatabaseUser();
            @NotNull final String password = databaseProperty.getDatabasePassword();
            @NotNull final String url = databaseProperty.getDatabaseUrl();
            @NotNull final Connection connection = DriverManager.getConnection(url, username, password);
            connection.setAutoCommit(false);
            return connection;
        } catch (@NotNull final SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
