package ru.t1.amsmirnov.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.IProjectTaskService;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ProjectNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.TaskNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.ProjectIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.TaskIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.model.Task;
import ru.t1.amsmirnov.taskmanager.repository.ProjectRepository;
import ru.t1.amsmirnov.taskmanager.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;


    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException, SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            if (!projectRepository.existById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @Nullable final Task task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            taskRepository.update(task);
        } catch (final SQLException exception) {
            connection.rollback();
            throw exception;
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException, SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            if (!projectRepository.existById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
            for (final Task task : tasks) taskRepository.removeOneById(userId, task.getId());
            projectRepository.removeOneById(userId, projectId);
        } catch (final SQLException exception) {
            connection.rollback();
            throw exception;
        } finally {
            connection.close();
        }
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException, SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(connection);
            if (!projectRepository.existById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = new TaskRepository(connection);
            @Nullable final Task task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            taskRepository.update(task);
        } catch (final SQLException exception) {
            connection.rollback();
            throw exception;
        } finally {
            connection.close();
        }
    }

    @NotNull
    public Connection getConnection() {
        return connectionService.getConnection();
    }

}
