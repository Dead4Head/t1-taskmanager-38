package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.Task;

public final class TaskUpdateByIdResponse extends AbstractTaskResponse {

    public TaskUpdateByIdResponse() {
    }

    public TaskUpdateByIdResponse(@Nullable final Task task) {
        super(task);
    }

    public TaskUpdateByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}