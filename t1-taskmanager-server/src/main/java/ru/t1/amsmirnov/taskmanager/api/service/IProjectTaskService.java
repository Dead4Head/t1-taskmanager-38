package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

import java.sql.SQLException;

public interface IProjectTaskService {

    void bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws AbstractException, SQLException;

    void removeProjectById(
            @Nullable String userId,
            @Nullable String projectId
    ) throws AbstractException, SQLException;

    void unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws AbstractException, SQLException;

}
