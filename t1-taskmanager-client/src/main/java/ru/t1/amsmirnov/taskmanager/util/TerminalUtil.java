package ru.t1.amsmirnov.taskmanager.util;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.field.NumberIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() throws AbstractException {
        @NotNull final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (@NotNull final Exception exception) {
            throw new NumberIncorrectException(value, exception);
        }
    }

}
