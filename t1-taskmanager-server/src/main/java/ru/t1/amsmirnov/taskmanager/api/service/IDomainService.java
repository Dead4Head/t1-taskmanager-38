package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

import java.sql.SQLException;

public interface IDomainService {

    @NotNull
    String FILE_BACKUP = "./backup.base64";

    void loadDataBackup() throws AbstractException, SQLException;

    void saveDataBackup() throws AbstractException, SQLException;

    void loadDataBase64() throws AbstractException, SQLException;

    void saveDataBase64() throws AbstractException, SQLException;

    void loadDataBin() throws AbstractException, SQLException;

    void saveDataBin() throws AbstractException, SQLException;

    void loadDataJsonFasterXML() throws AbstractException, SQLException;

    void saveDataJsonFasterXML() throws AbstractException, SQLException;

    void loadDataJsonJaxB() throws AbstractException, SQLException;

    void saveDataJsonJaxB() throws AbstractException, SQLException;

    void loadDataXMLFasterXML() throws AbstractException, SQLException;

    void saveDataXMLFasterXML() throws AbstractException, SQLException;

    void loadDataXMLJaxB() throws AbstractException, SQLException;

    void saveDataXMLJaxB() throws AbstractException, SQLException;

    void loadDataYAMLFasterXML() throws AbstractException, SQLException;

    void saveDataYAMLFasterXML() throws AbstractException, SQLException;

}
