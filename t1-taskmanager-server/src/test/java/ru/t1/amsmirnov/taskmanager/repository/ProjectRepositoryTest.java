package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.enumerated.ProjectSort;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.model.Project;
import ru.t1.amsmirnov.taskmanager.service.ConnectionService;
import ru.t1.amsmirnov.taskmanager.service.PropertyService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String NONE_ID = "---NONE---";

    @NotNull
    private static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(new PropertyService());

    @NotNull
    private Connection connection;

    @NotNull
    private List<Project> projects;

    @NotNull
    private List<Project> alfaProjects;

    @NotNull
    private List<Project> betaProjects;

    @NotNull
    private IProjectRepository projectRepository;

    @Before
    public void initRepository() throws SQLException {
        connection = CONNECTION_SERVICE.getConnection();
        projects = new ArrayList<>();
        projectRepository = new ProjectRepository(connection);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project " + (NUMBER_OF_ENTRIES - i));
            project.setDescription("Description " + i);
            if (i < 5)
                project.setUserId(USER_ALFA_ID);
            else
                project.setUserId(USER_BETA_ID);
            try {
                projectRepository.add(project);
                projects.add(project);
            } catch (final SQLException e) {
                connection.rollback();
                throw e;
            }
        }
        projects = projects.stream()
                .sorted(ProjectSort.BY_CREATED.getComparator())
                .collect(Collectors.toList());
        alfaProjects = projects
                .stream()
                .filter(p -> p.getUserId().equals(USER_ALFA_ID))
                .collect(Collectors.toList());
        betaProjects = projects
                .stream()
                .filter(p -> p.getUserId().equals(USER_BETA_ID))
                .collect(Collectors.toList());
    }

    @After
    public void clearRepository() throws SQLException {
        try {
            projectRepository.removeAll();
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
        projects.clear();
    }

    @Test
    public void testAdd() throws SQLException {
        @NotNull final Project project = new Project();
        project.setName("Added project");
        project.setDescription("Added description");
        project.setUserId(USER_ALFA_ID);
        try {
            projectRepository.add(USER_ALFA_ID, project);
            projects.add(project);
            @Nullable final List<Project> actualProjectList = projectRepository.findAll();
            assertEquals(projects, actualProjectList);
        } catch (final SQLException e) {
            connection.close();
            throw e;
        }
    }

    @Test
    public void testRemoveAll() throws SQLException {
        try {
            assertNotEquals(0, projectRepository.getSize());
            projectRepository.removeAll();
            assertEquals(0, projectRepository.getSize());
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveAllForUser() throws SQLException {
        try {
            assertNotEquals(0, projectRepository.getSize(USER_ALFA_ID));
            projectRepository.removeAll(USER_ALFA_ID);
            assertEquals(0, projectRepository.getSize(USER_ALFA_ID));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testExistById() throws SQLException {
        try {
            for (final Project project : projects) {
                assertTrue(projectRepository.existById(project.getUserId(), project.getId()));
            }
            assertFalse(projectRepository.existById(USER_ALFA_ID, NONE_ID));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testFindAll() throws SQLException {
        try {
            List<Project> actualAlfaProjects = projectRepository.findAll(USER_ALFA_ID);
            List<Project> actualBetaProjects = projectRepository.findAll(USER_BETA_ID);
            assertNotEquals(actualAlfaProjects, actualBetaProjects);
            assertEquals(alfaProjects, actualAlfaProjects);
            assertEquals(betaProjects, actualBetaProjects);
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Ignore
    public void testFindAllComparator() throws SQLException {
        try {
            @NotNull final Comparator<Project> comparator = ProjectSort.BY_NAME.getComparator();
            List<Project> actualAlfaProjects = projectRepository.findAll(USER_ALFA_ID, comparator);
            alfaProjects = alfaProjects.stream().sorted(comparator).collect(Collectors.toList());
            assertEquals(alfaProjects, actualAlfaProjects);
            projects = projects.stream().sorted(comparator).collect(Collectors.toList());
            assertEquals(projects, projectRepository.findAll(comparator));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testFindOneById() throws SQLException {
        try {
            for (final Project project : projects) {
                assertEquals(project, projectRepository.findOneById(project.getUserId(), project.getId()));
            }
            assertNull(projectRepository.findOneById(USER_ALFA_ID, NONE_ID));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testFindOneByIndex() throws SQLException {
        try {
            for (int i = 0; i <= projects.size(); i++)
                assertEquals(projects.get(0), projectRepository.findOneByIndex(0));
            for (int i = 0; i < alfaProjects.size(); i++)
                assertEquals(alfaProjects.get(i), projectRepository.findOneByIndex(USER_ALFA_ID, i));
            for (int i = 0; i < betaProjects.size(); i++)
                assertEquals(betaProjects.get(i), projectRepository.findOneByIndex(USER_BETA_ID, i));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testGetSize() throws SQLException {
        try {
            assertEquals(projects.size(), projectRepository.getSize());
            assertEquals(alfaProjects.size(), projectRepository.getSize(USER_ALFA_ID));
            assertEquals(betaProjects.size(), projectRepository.getSize(USER_BETA_ID));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveOne() throws SQLException {
        try {
            for (final Project project : projects) {
                assertTrue(projectRepository.existById(project.getId()));
                projectRepository.removeOne(project.getUserId(), project);
                assertFalse(projectRepository.existById(project.getId()));
            }
            assertNull(projectRepository.removeOne(USER_ALFA_ID, new Project()));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveOneById() throws SQLException {
        try {
            for (final Project project : projects) {
                assertTrue(projectRepository.existById(project.getId()));
                projectRepository.removeOneById(project.getUserId(), project.getId());
                assertFalse(projectRepository.existById(project.getId()));
            }
            assertNull(projectRepository.removeOneById(USER_BETA_ID, NONE_ID));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void testRemoveOneByIndex() throws SQLException {
        try {
            for (final Project project : alfaProjects) {
                assertTrue(projectRepository.existById(project.getId()));
                projectRepository.removeOneByIndex(project.getUserId(), 0);
                assertFalse(projectRepository.existById(project.getId()));
            }
            for (final Project project : betaProjects) {
                assertTrue(projectRepository.existById(project.getId()));
                projectRepository.removeOneByIndex(project.getUserId(), 0);
                assertFalse(projectRepository.existById(project.getId()));
            }
            assertNull(projectRepository.removeOneByIndex(USER_ALFA_ID, 10));
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

}
