package ru.t1.amsmirnov.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.amsmirnov.taskmanager.api.repository.ISessionRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.marker.DBCategory;
import ru.t1.amsmirnov.taskmanager.model.Session;
import ru.t1.amsmirnov.taskmanager.service.ConnectionService;
import ru.t1.amsmirnov.taskmanager.service.PropertyService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@Category(DBCategory.class)
public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String NONE_ID = "---NONE---";

    @NotNull
    private static final String USER_ALFA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_BETA_ID = UUID.randomUUID().toString();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(new PropertyService());

    @NotNull
    private static final Date TODAY = new Date();

    @NotNull
    private final List<Session> sessions = new ArrayList<>();

    private ISessionRepository sessionRepository;

    private Connection connection;

    @Before
    public void initRepository() throws Exception {
        connection = CONNECTION_SERVICE.getConnection();
        sessionRepository = new SessionRepository(connection);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            Thread.sleep(2);
            @NotNull final Session session = new Session();
            session.setCreated(TODAY);
            if (i <= 5)
                session.setUserId(USER_ALFA_ID);
            else
                session.setUserId(USER_BETA_ID);
            try {
                sessions.add(session);
                sessionRepository.add(session);
            } catch (final SQLException e) {
                connection.rollback();
                throw e;
            }
        }
    }

    @After
    public void clearRepository() throws SQLException {
        try {
            sessionRepository.removeAll();
            sessions.clear();
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Test
    public void addAllTest() throws SQLException {
        ArrayList<Session> newSessions = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session newSession = new Session();
            newSession.setCreated(new Date());
            newSessions.add(newSession);
        }
        try {
            sessionRepository.addAll(newSessions);
            sessions.addAll(newSessions);
            assertEquals(sessions, sessionRepository.findAll());
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void setTest() throws SQLException {
        ArrayList<Session> newSessions = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session newSession = new Session();
            newSession.setCreated(new Date());
            newSessions.add(newSession);
        }
        try {
            sessionRepository.set(newSessions);
            sessions.clear();
            sessions.addAll(newSessions);
            assertEquals(sessions, sessionRepository.findAll());
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void findOneByIndexTest() throws SQLException {
        try {
            assertNull(sessionRepository.removeOneByIndex(sessions.size()));
            assertEquals(sessions.get(0), sessionRepository.removeOneByIndex(0));
            sessions.remove(0);
            assertEquals(sessions, sessionRepository.findAll());
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test(expected = SQLException.class)
    public void findOneByIndexTest_Exception() throws SQLException {
        try {
            sessionRepository.removeOneByIndex(-1);
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    public void removeOneByIdTest() throws SQLException {
        try {
            assertNull(sessionRepository.removeOneById(NONE_ID));
            for (Session session : sessions) {
                assertEquals(session, sessionRepository.removeOneById(session.getId()));
            }
            sessions.clear();
            assertEquals(sessions, sessionRepository.findAll());
        } catch (final SQLException e) {
            connection.rollback();
            throw e;
        }
    }

}
