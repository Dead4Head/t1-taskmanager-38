package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.AbstractUserOwnedModel;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    @NotNull
    M add(@Nullable String userId, @Nullable M model) throws AbstractException, SQLException;

    @NotNull
    List<M> findAll(@Nullable String userId) throws AbstractException, SQLException;

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator) throws AbstractException, SQLException;

    @NotNull
    M findOneById(@Nullable String userId, @Nullable String id) throws AbstractException, SQLException;

    @NotNull
    M findOneByIndex(@Nullable String userId, @Nullable Integer index) throws AbstractException, SQLException;

    @NotNull
    M removeOne(@Nullable String userId, @Nullable M model) throws AbstractException, SQLException;

    @NotNull
    M removeOneById(@Nullable String userId, @Nullable String id) throws AbstractException, SQLException;

    @NotNull
    M removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws AbstractException, SQLException;

    boolean existById(@Nullable String userId, @Nullable String id) throws AbstractException, SQLException;

    void removeAll(@Nullable String userId) throws AbstractException, SQLException;

    int getSize(@Nullable String userId) throws AbstractException, SQLException;

}
