package ru.t1.amsmirnov.taskmanager.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class  Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() throws AbstractException, SQLException {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        try {
            bootstrap.getDomainService().saveDataBackup();
        } catch (final Exception exception) {
            bootstrap.getLoggerService().error(exception);
        }
    }

    public void load() throws AbstractException, SQLException {
        if (!Files.exists(Paths.get(bootstrap.getDomainService().FILE_BACKUP))) return;
        bootstrap.getDomainService().loadDataBackup();
    }

}
