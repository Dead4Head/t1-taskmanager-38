package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.List;

public abstract class AbstractTaskListResponse extends AbstractResultResponse {

    @Nullable
    private List<Task> tasks;
    protected AbstractTaskListResponse() {

    }

    protected AbstractTaskListResponse(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

    protected AbstractTaskListResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

    @Nullable
    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

}