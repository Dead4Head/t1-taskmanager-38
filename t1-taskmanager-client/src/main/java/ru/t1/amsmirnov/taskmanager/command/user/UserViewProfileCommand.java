package ru.t1.amsmirnov.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserProfileRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.user.UserProfileResponse;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-view-profile";

    @NotNull
    public static final String DESCRIPTION = "Shows user profile.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER PROFILE]");
        @NotNull UserProfileRequest request = new UserProfileRequest(getToken());
        @NotNull UserProfileResponse response = getUserEndpoint().viewProfile(request);
        if (!response.isSuccess() || response.getUser() == null)
            throw new CommandException(response.getMessage());
        showUser(response.getUser());
    }

}
