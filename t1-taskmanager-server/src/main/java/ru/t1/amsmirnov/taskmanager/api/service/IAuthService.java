package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.Session;
import ru.t1.amsmirnov.taskmanager.model.User;

import java.sql.SQLException;

public interface IAuthService {

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email) throws AbstractException, SQLException;

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws AbstractException, SQLException;

    void logout(@Nullable Session session) throws AbstractException, SQLException;

    @NotNull
    Session validateToken(@Nullable String token) throws AbstractException;

}
